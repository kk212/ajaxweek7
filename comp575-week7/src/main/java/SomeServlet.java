

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * Servlet implementation class SomeServlet
 */
public class SomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		 
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		File imagesDir = new File(getServletContext().getRealPath("/images"));
		File[] imageFiles = imagesDir.listFiles();
		
		Random generator = new Random();
		int num = generator.nextInt(imageFiles.length-1);// random int for the index of the array
		String fileName=imageFiles[num].getName(); // get name of the file from the array, using random number from num as an index 
		
		response.setCharacterEncoding("UTF-8"); //encoding
		response.setContentType("text"); // tell the content of the response
		response.getWriter().write(fileName);//send the string to the response.
	}

}
